const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const port = 3000;

app.use(express.static('frontend/dist'));
app.use(bodyParser.json());

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

app.get('/', (req, res) =>
  res.sendFile(path.join(__dirname + 'frontend/dist/index.html')));

app.post('/api/v1/user', (req, res) => {
  console.log(req.body);
  res.json({res: 'test'});
})

/*app.get('/api/v1', (req, res) =>
  res.json({test: 'test'})); */

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
