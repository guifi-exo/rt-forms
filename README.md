# noc-forms
This repository stores forms & api ready to deal with user requests (new subscriptions, issues, unsubscriptions...)

This system is based on an API (acting as middleware between ticketing software and end user) and some web forms (using Vue.js framework).

## development install

    npm install
    cd frontend
    npm install

to develop it is necessary to use vue cli:

    sudo npm install -g @vue/cli

https://cli.vuejs.org/guide/installation.html

to start backend server (including builded frontend) use:
    npm run start

## production install

build frontend:

    npm run build-frontend

## TODOS
### Backend
- Define a configuration file to configure both services (Backend & Frontend)
- Divide code in chunks (when it increses size)
- Prepare systemd unit daemon
- Connect to RT API

### Frontend
- Connect frontend configuration in order to get baseUrl parameter
- Setup a frontend router (in order to have routed views)
- Discuss reCaptcha or similar to avoid spam