import Vue from 'vue';
import VueRouter from 'vue-router'
import App from './App.vue';
import BootstrapVue from 'bootstrap-vue';
import axios from 'axios';

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css';

// View components
import NewUser from "@/components/NewUser.vue";

Vue.config.productionTip = false;

Vue.use(BootstrapVue);
Vue.use(VueRouter);

//TODO (baseURL) get from an ENV variable or config file
//TODO Global axios instance
/*Object.defineProperties(Vue.prototype, {
  $axios: axios.create({
    baseURL: `http://localhost:3000/api/v1/`
  })
})*/

const routes = [
  {
    path: '/',
    name: 'NewUser',
    component: NewUser
  }
]

const router = new VueRouter({
  routes // short for `routes: routes`
})

axios.defaults.baseURL = "http://localhost:3000/api/v1/"

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
